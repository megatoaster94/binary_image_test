#include <iostream>
#include <iomanip>
#ifdef WIN32
#include "include/opencv2/imgproc.hpp"
#include "include/opencv2/highgui.hpp"
#else
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#endif
#include <omp.h>
#include <chrono>
#include <thread>

#define USAGE usage(argv[0])

void usage(char *program)
{
    std::cout << "Usage: " << program << " image [threads]" << std::endl;
    std::cout << "\timage: path to image" << std::endl;
    std::cout << "\tthread (optional): amount of threads" << std::endl;
}

cv::Mat grayscale_image(cv::Mat original_image, int threads)
{
    cv::Mat gray_image(original_image.size(), CV_8UC1);

    int rows = original_image.rows;
    int cols = original_image.cols;
#pragma omp parallel for collapse(2) num_threads(threads)
    for(int i = 0; i < rows; ++i)
    {
        for(int j = 0; j < cols; ++j)
        {
            cv::Vec3b intensity = original_image.at<cv::Vec3b>(i, j);

            int blue = intensity.val[0];
            int green = intensity.val[1];
            int red = intensity.val[2];

            gray_image.at<uchar>(i, j) = blue*0.0722 + green*0.7152 + red*0.2126;
        }
    }
    return gray_image;
}

void calculate_hist(cv::Mat image, int *hist, int threads)
{
    int rows = image.rows;
    int cols = image.cols;
#pragma omp parallel for collapse(2) num_threads(threads)
    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {
            hist[image.at<uchar>(i, j)]++;
        }
    }
}

int64 calculate_intensity_sum(cv::Mat image, int threads)
{
    int64 sum = 0;
    int rows = image.rows;
    int cols = image.cols;
#pragma omp parallel for collapse(2) num_threads(threads)
    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {
            sum += image.at<uchar>(i, j);
        }
    }
    return sum;
}

int otsu_threshold(cv::Mat image, int threads)
{
    int hist[256] = {0};
    calculate_hist(image, hist, threads);

    std::cout << "Hist: " << std::endl;
    for (size_t i = 0; i < 256; i++)
    {
        std::cout << hist[i] << " ";
    }
    std::cout << std::endl;

    int64 intensity_sum = calculate_intensity_sum(image, threads);

    std::cout << "Intensity sum: " << intensity_sum << std::endl;

    int best_thresh = 0;
    double best_sigma = 0.0;

    int first_class_pixel_count = 0;
    int first_class_intensity_sum = 0;

    int pixel_count = image.rows * image.cols;

#pragma omp parallel for num_threads(threads)
    for (int thresh = 0; thresh < 255; ++thresh) {
        first_class_pixel_count += hist[thresh];
        first_class_intensity_sum += thresh * hist[thresh];

        double first_class_prob = first_class_pixel_count / (double) pixel_count;
        double second_class_prob = 1.0 - first_class_prob;

        double first_class_mean = first_class_intensity_sum / (double) first_class_pixel_count;
        double second_class_mean = (intensity_sum - first_class_intensity_sum)
                / (double) (pixel_count - first_class_pixel_count);

        double mean_delta = first_class_mean - second_class_mean;

        double sigma = first_class_prob * second_class_prob * mean_delta * mean_delta;

        if (sigma > best_sigma) {
            best_sigma = sigma;
            best_thresh = thresh;
        }
    }

    return best_thresh;
}

cv::Mat binarize_image(cv::Mat original_image, int threads)
{
    cv::Mat binary_image(original_image);

    int rows = original_image.rows;
    int cols = original_image.cols;

    int threshold = otsu_threshold(binary_image, threads);

#pragma omp parallel for collapse(2) num_threads(threads)
    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {
            uchar intensity = binary_image.at<uchar>(i, j);
            if (intensity > threshold)
            {
                binary_image.at<uchar>(i, j) = 255;
            }
            else
            {
                binary_image.at<uchar>(i, j) = 1;
            }
        }
    }
    return binary_image;
}

void timer(std::chrono::time_point<std::chrono::high_resolution_clock> start, std::string message)
{
    const auto finish = std::chrono::high_resolution_clock::now();
    auto duration = finish - start;
    const auto min = std::chrono::duration_cast< std::chrono::minutes > ( duration );
    duration -= min;
    const auto sec = std::chrono::duration_cast< std::chrono::seconds > ( duration );
    duration -= sec;
    const auto milli = std::chrono::duration_cast< std::chrono::milliseconds > ( duration );
    std::cout << message
              << min.count() << " m "
              << sec.count() << "."
              << std::setw( 3 ) << std::setfill( '0' ) << milli.count() << " s\n";
}



int main(int argc, char *argv[])
{
    if (argc == 2 || argc == 3)
    {
        const auto start = std::chrono::high_resolution_clock::now();

        //int threads = std::thread::hardware_concurrency();
        int threads = omp_get_max_threads();
        std::cout << "Number of threads: " << threads << std::endl;
        if (argc == 3)
            threads = atoi(argv[2]);
        if (threads == 0)
            threads = 1;

        cv::Mat rgb_image;
        std::string rgb_image_filename(argv[1]);
        rgb_image = cv::imread(rgb_image_filename);

        if (rgb_image.empty())
        {
            std::cout << "Error loading image." << std::endl;
            return 1;
        }

        cv::Mat gray_image = grayscale_image(rgb_image, threads);

        cv::Mat binary_image = binarize_image(gray_image, threads);

        timer(start, "Time remaining: ");

        cv::namedWindow("Before", cv::WINDOW_NORMAL);
        cv::resizeWindow("Before", rgb_image.rows/2, rgb_image.cols/2);
        cv::imshow("Before", rgb_image);
        cv::waitKey(0);

        cv::namedWindow("After", cv::WINDOW_NORMAL);
        cv::resizeWindow("After", rgb_image.rows/2, rgb_image.cols/2);
        cv::imshow("After", binary_image);
        cv::waitKey(0);

        std::string rawname = rgb_image_filename.substr(0, rgb_image_filename.find_last_of("."));
        std::string extension = rgb_image_filename.substr(rgb_image_filename.find_last_of("."));
        std::string binary_image_filename(rawname + "_binarized" + extension);
        imwrite(binary_image_filename, binary_image);
    }
    else
    {
        USAGE;
        return 1;
    }

    return 0;

}
